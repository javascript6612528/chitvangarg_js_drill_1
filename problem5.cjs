const problem5 = function(inventory, years){
    const filtered_cars = [];

    for(let i=0; i<years.length; i++){
        if(years[i] < 2000){
            filtered_cars.push(inventory[i])
        }
    }
    
    return filtered_cars;
}

module.exports = problem5