const problem2 = function(inventory){
    const ans = `Last car is a ${inventory[inventory.length-1]['car_make']} ${inventory[inventory.length-1]['car_model']}`
    return ans;
}

module.exports = problem2