
const problem1 = function(inventory){
    for(let i=0; i<inventory.length; i++){
        if(inventory[i].id === 33){
            var ans = `Car 33 is a ${inventory[i].car_year} ${inventory[i].car_make} ${inventory[i].car_model}`;
            return ans;
        }
    }
}

module.exports = problem1

