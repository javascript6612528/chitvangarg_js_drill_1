const problem5 = function(inventory){
    const filtered_cars = [];

    for(let i=0; i<inventory.length; i++){
        if(inventory[i]['car_make'] === 'BMW' || inventory[i]['car_make'] === 'Audi'){
            filtered_cars.push(inventory[i])
        }
    }
    
    return filtered_cars;
}

module.exports = problem5